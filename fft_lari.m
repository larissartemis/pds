function y = fft_lari(x)
   x = x(:); % s� pra garantir que a entrada � um vetor coluna
   n = length(x);
   omega = exp(2*pi*i/n);
   if rem(n,2) == 0 % enquanto o resto da divis�o for par
      % Recursive divide and conquer
      k = (0:n/2-1)';
      w = omega .^ k;
      u = fft_lari(x(1:2:n-1)); % chama a fft s� com os fatores �mpares
      v = w.*fft_lari(x(2:2:n)); % chama a fft s� com os fatores pares
      y = [u+v; u-v];
   else % caso base, o tamanho da entrada � 1
      j = 0:n-1;
      k = j';
      F = omega .^ (k*j);
      y = F*x;
   end