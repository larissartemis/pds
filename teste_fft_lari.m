x = [1 2 3 4]; % entrada
y_lari = fft_lari(x);
% o matlab retorna o resultado da fft em um vetor linha
% ent�o, pra facilitar a compara��o dos resultados:
y_lari = y_lari' 
% o c�lculo do matlab
y_matlab = fft(x)

% OBSERVA��O
% isequal(y_lari, y_matlab) n�o funciona, por alguma diferen�a de
% aproximacao provavelmente, ent�o a compara��o das duas pode ser feita
% visualmente, olhando termo a termo, ou mostrando que elas s�o muito 
% pr�ximas (com o exemplo de entrada, a diferen�a � da ordem de 10^(-15)
dif = y_lari - y_matlab
